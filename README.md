Dr. Omar, DDS, has a state of the art practice on the Upper West Side of Manhattan. She offers general and cosmetic dentistry including teeth whitening and veneers. She understands that seeing a dentist can be stressful, and spends a lot of effort making sure that you are relaxed and pain free.

Address: 50 Riverside Blvd, Unit A, New York, NY 10069, USA

Phone: 646-844-2100

Website: https://zaradentalnyc.com
